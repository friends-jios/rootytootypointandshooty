extends Node2D


var enemy_scene = load("res://Scenes/Strait_Shot_Enemy.tscn")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.set_wait_time(3)
	$Timer.start()
	
func spawn_enemy():
	var enemy = enemy_scene.instance()
	randomize()
	enemy.position.x = rand_range(0,600)
	add_child(enemy)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func timeout():
	spawn_enemy()
