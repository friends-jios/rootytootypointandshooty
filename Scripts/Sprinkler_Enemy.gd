extends Node2D

var bullet_scene = load("res://Scenes/Bullet.tscn")
export var health = 1
var type = "ENEMY"
export var enemy = "SPRINKLER"

func _ready():
	$Timer.set_wait_time(.2)
	$Timer.start()
	var target = Vector2(self.position.x, self.position.y + 100)
	#$Move_Tween.interpolate_property(self, "position", position, target, 3, Tween.TRANS_QUINT, Tween.EASE_OUT)
	#$Move_Tween.start()

func _process(delta):
	rotate(1 * delta)
	#position.y += 100 * delta
	
	if (health <= 0 || position.y > get_viewport_rect().size.y + 20):
		get_parent().remove_child(self)
		queue_free()
		
func spawn_bullets():
	var b1 = bullet_scene.instance()
	b1.position = self.position
	b1.rotation = self.rotation
	b1.dir = Vector2(0, 1)
	var b2 = bullet_scene.instance()
	b2.position = self.position
	b2.rotation = self.rotation
	b2.dir = Vector2(0, -1)
	#b3.dir = Vector2(1, 0).normalized()
	#b4.dir = Vector2(-1, 0).normalized()
	
	get_parent().add_child(b1)
	get_parent().add_child(b2)

func timeout():
	spawn_bullets()
