extends Node2D

var health_status = [
	"You died", 
	"Danger!", 
	"Could be worse", 
	"Don\'t worry about it",
	"No sweat", 
	"You\'re doing great!"
]

var player_score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	

