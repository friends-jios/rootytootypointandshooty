extends Node2D

var dir = Vector2(1, 0)
export var bullet_speed = 300
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func define_creator(arg1):
	print(arg1)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#if (get_parent().type == "ENEMY" && get_parent().enemy == "Sprinkler"):
		#pass
	#if (a == "SPRINKLER"):
		#pass
	#else:
	self.position += dir * delta * bullet_speed
	if($RayCast2D.is_colliding()):
		var collid = $RayCast2D.get_collider().get_parent()
		if (collid.type == "PLAYER"):
			position += Vector2(2000, 2000)
			if (collid.health > 0):
				collid.health -= 1
				collid.advance_status()


func screen_exited():
	queue_free()
