extends Node2D

var bullet_scene = load("res://Scenes/Bullet.tscn")
onready var player = get_parent().get_parent().get_node("Player")
var type = "ENEMY"
export var health = 1

func _ready():
	$Timer.set_wait_time(.2)
	$Timer.start()
	var target = Vector2(self.position.x, self.position.y + 100)
	$Move_Tween.interpolate_property(self, "position", position, target, 3, Tween.TRANS_QUINT, Tween.EASE_OUT)
	$Move_Tween.start()


func _process(delta):
	rotate(1 * delta)
	position.y += 100 * delta
	
	if (health <= 0 || position.y > get_viewport_rect().size.y + 20):
		get_parent().remove_child(self)
		queue_free()
		
func spawn_bullets():
	var b1 = bullet_scene.instance()
	b1.define_creator("STRAIT")
	b1.position = self.position
	#b1.rotation = self.rotation
	b1.dir = Vector2(player.position.x - self.position.x, player.position.y - self.position.y).normalized()
	
	get_parent().add_child(b1)

func timeout():
	spawn_bullets()
