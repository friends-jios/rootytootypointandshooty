extends Node2D

var type = "PLAYER"
var p_bullet = load("res://Scenes/P_Bullet.tscn")
export var health = 5
var player_score = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.set_wait_time(.1)
	$Timer.start()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.position = get_global_mouse_position()
	if health <= 0:
		get_tree().change_scene("res://Scenes/MainMenu.tscn")
func shoot_bullet():
	var b = p_bullet.instance()
	get_parent().add_child(b)
	b.position = self.position
	
func advance_status():
	get_parent().get_node("HealthContainer").get_node("HealthLabel").text = get_parent().health_status[health]
	
func advance_score():
	player_score += 1
	get_parent().get_node("ScoreContainer").get_node("ScoreValue").text = str(player_score)

func timeout():
	if (Input.is_action_pressed("shoot")):
		shoot_bullet()
	pass # Replace with function body.
